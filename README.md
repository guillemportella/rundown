# Rundown: visually explore projects quality control

Simple `Flask` web server and API to store and display quality control 
reports (`QC`) from genomic analysis.
Typical application involves automatically generating `QC` reports 
using `MultiQC` and programmatically uploading them to the server via 
the web API.

Stores `MultiQC` reports based on project name and run name (i.e. different 
analysis/runs) for a given user. Within each project/run, you can 
store different `QC` files, provided that their name is unique. The `html`
reports are stored in `STORE_FOLDER`, and the database keeps a reference
to them. With some (minimal) modifications of the code, they could also be
served from a remote location. Right now `flask` is serving them, but I 
will explore using `nginx` to serve them as assets, which will be much faster.

You can also post a single file for *one off* display, which will not be 
stored in the database, and will be overwritten if you submit another file.

This here is just the code for the back-end API, which 
is used to post data and query the database of project/runs/reports. You can 
interact with the API via `POST` and `GET`, but due to authentication via
cookies and certificates it's more practical to use script using a decent
library (e.g. python's `requests` library). There is an example in `submit_report.py`
script in `/utils/`.

> The front end code is in the "rundown_frontend" repository, which I'm keeping private
for the moment. The `minified` version of the front end can be served directly from `nginx`, 
more documentation will follow.


----

To access the site and to use the `API`, the user must have a username and 
a password stored in the database of users. To avoid placing the password in
scripts that automatize uploading the reports, the API used to provide and endpoint
that generates user tokens. This has been changed recently to allow for a 
single page application in the front end. 

The credentials are now implemented as tokens and `httponly` cookies, 
using a double identification system and JSON Web Tokens (JWT). Interacting with 
the API that required cookis is a bit of a pain using only `curl` and the like, so 
I have a `submit_report.py` script in `utils` that shows how to do it with 
`python`. 

----

## Usage

Clone this repository, install the dependencies and start the server.
Whereas you can certainly install all the dependencies via `pip`, 
it is recommended to create a local environment using `pipenv`.

For production, the use of `gunicorn` on top of the `Flask` server is 
recommended.

The configuration is managed via environment variables via `dotenv` module. 
You can definitely export all the required variables by hand, but the most
sensible approach is to have a `.env` file in `src/rundown` defining the 
following keys. 

```
DEBUG=False
TESTING=False
TOKEN_EXPIRATION=86400
SECRET_KEY = 'your_secret_key'
SQLALCHEMY_TRACK_MODIFICATIONS=False
STORE_FOLDER="/full_path_to_storage_folder"
UPLOAD_FOLDER=STORE_FOLDER
QD_HTML_NAME="qd_show.html"

DATABASE_PWD_URL='sqlite:///passwd.db'
DATABASE_PROJECTS_USR='sqlite:///projects.db'
``` 

> NB: **Do not** use DEBUG=True in production

For example, if you were to use 4 processes in the server, you'd run

```bash
git clone git@gitlab.com:guillemportella/rundown.git
cd rundown
# right now use the develop, will merge to master soon
git checkout develop
pipenv install --dev --ignore-pipfile 

# TODO: after merging with master, update me

# Highly recommened: run the tests before starting production, won't take long
pipenv run pytest -v -s

# start the server
pipenv run gunicorn -w 4 src.rundown.wsgi:app
```

> Decent `pipenv` [tutorial](https://realpython.com/pipenv-guide/#pipenv-introduction)

This will start the web server and API. Right now, registered users are 
store in the `sqlite3` `passwd.db` file and can not be added from the web
interface. 

The API uses `Swagger` to document its endpoints, you can access the 
documentation in `hostname:port/api/doc`. 

# Using `circusctl` to orchestrate 

Right now it's relatively easy to manage the app, but if you'd include a 
an external database, or add other features, it's nice to be able to 
control the deployment. I include a `.ini` file for `circusctl` in the 
repository, this is how I run it 

```bash
circusd circus_server.ini --daemon --log-output /tmp/circus.tmp
```

then you can query its state, restart, etc., like 

```
circusctl status 
```

# Requirements

Python 3.6 and above (?), `gunicorn`, `pytests` and a bunch of dependencies
specified in `requirements.txt`. Using `pipenv` is compulsory right now.

```
flask
dotenv
dotenv
pytest
requests
flask-migrate
gunicorn
flask-debugtoolbar
flask-bcrypt
flask-login
flask-restplus
flask-sqlalchemy
flask-marshmallow
flask-bootstrap
marshmallow-sqlalchemy
pytest-factoryboy
requests
pytest
sqlalchemy
werkzeug
```

Right now the database is simply `sqlite3`, but we can easily use any other 
, more performant, `SQL` database, e.g. `Postgres`, since we are 
using `sqlalchemy` as python interface. 


