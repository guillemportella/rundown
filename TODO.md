# Todo

1. Gracefully deny starting the app if env variables not defined
11. Integrate create user and submit_report as script of the app, to use updated DB fields
1. Add swagger docs to API, via flask_restplus
3. Comment a bit more the source code, add more type annotations if possible
4. Front end... keep it simple.
44. Add tests to follow links in the website and report back.
5. Set up in server.
6. Add utility scripts to create users / post projects
7. Clean up requirements, fix installation and set up.
8. Add examples and how-to in readme.
9. Streamline deploy (CI?)
10. Serve assets directly via `nginx` ??
11. Add the ability to rename entries 

# Done

1. ~~Implement database migrations~~ 
2. ~~Add a readme.md~~ -> Done, but needs updating.
2. ~~The test files should be deleted after testing is done.~~
2. ~~Configure logger to follow DEBUG env variable~~ 
