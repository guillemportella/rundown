from setuptools import find_packages
from setuptools import setup

setup(
    name='rundown',
    python_requires='>3.6',
    version="0.0.1",
    packages=find_packages('src'),
    include_package_data=True,
    package_dir={'': 'src'},
    install_requires=[
        'flask', 'dotenv', 'dotenv', 'pytest', 'requests', 'flask-migrate',
        'gunicorn', 'flask-debugtoolbar', 'flask-bcrypt', 'flask-login',
        'flask-restplus', 'flask-sqlalchemy', 'flask-marshmallow',
        'flask-bootstrap', 'marshmallow-sqlalchemy', 'pytest-factoryboy',
        'requests', 'pytest', 'sqlalchemy', 'werkzeug',
    ],

    package_data={
        # If any package contains *.txt or *.rst files, include them:
        'package_data': ['*.html'],
    },

    # metadata for upload to PyPI
    license="MIT",
    keywords="View my running jobs",
    project_urls={
        "Source Code": "https://gitlab.com/guillemportella/rundown.git",
    }
)
