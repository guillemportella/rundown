#! /usr/bin/env python3
"""App."""
import logging

from flask import Flask
from flask_bcrypt import Bcrypt
from flask_bootstrap import Bootstrap
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_login import LoginManager
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from .logging_config import config_logging

logging.config.dictConfig(config_logging.LOG_SETTINGS)
logger = logging.getLogger('flask_logger')

# the DB, bcrypt and LoginManagers are for everyone
db = SQLAlchemy()
bcrypt = Bcrypt()
jwt = JWTManager()
login_manager = LoginManager()


# application factory
def create_app(config_obj):
    """The application factor for this app.

    # First and foremost!:
    # *Caution* with flaks-restplus (the api_bp blueprint)!!
    # Right now (flask-restplus==0.11.0), adds "/" to the url_map
    # from werkzeug, with the nasty effect that it might overwrite the
    # redirect I have in views_bp.route("/") to point to the login page.
    # Solutions I found so far:
    # 1) --> register api_bp after views_bp, the right one will precede
    # 2) --> define a "prefix" when creating Api.
    #        you will still have a dangling route, that
    #        of the prefix, but at least it's not the landing page
    # I have done both things, just in case I forget later on
    #
    # To view the registered routes, you iterate app.url_map.iter_rule()

    """
    app = Flask(__name__)
    app.config.from_object(config_obj)

    db.init_app(app)
    with app.app_context():
        db.create_all()
        db.create_all(bind='projects_db')
        login_manager.init_app(app)
        login_manager.login_view = 'views_bp.login'
        bootstrap = Bootstrap(app)
        _ = Marshmallow(app)
        db.session.commit()
        from .public import (views_bp, api_bp)
        from .auth.views import auth_bp
        app.register_blueprint(views_bp)
        app.register_blueprint(api_bp)
        app.register_blueprint(auth_bp)
        db.create_all(bind='projects_db')
        db.session.commit()
    _ = Bcrypt(app)
    jwt.init_app(app)
    cors = CORS(app, supports_credentials=True,
                resources={r"/api/*": {"origins": "*"},
                           r"/auth/*": {"origins": "*"}})
    migrate = Migrate(app, db)

    logger.debug("These are all your url routes")
    for r in app.url_map.iter_rules():
        logger.debug(r)
    return app


@login_manager.user_loader
def load_user(user_id):
    """Load user by ID."""
    from .models.users import User
    return User.query.get(int(user_id))

# @login_manager.request_loader
# def load_user_from_request(request):
#     """ Authenticate using the API
#
#     IMPORTANT: This function must return either a User instance or None.
#
#     This decorated function is used by views that have a @login_required
#     decorated. A given API view that requires identification either accepts
#     a username:pwd or a token:unused.
#
#     eg.
#
#     curl -u username:pwd http://server/api/gen_token
#     curl -u token:unused http://server/api/gen_token
#
#     (here "unused" is just a place holder, is needed by curl won't
#     be used at all)
#
#      """
#     logger.info("request_loader trying to login a user.")
#     # could be that there's not header at all
#
#     from .models.users import User
#     logger.debug(request)
#     if request.headers.get('Authorization') is None:
#         logger.info("The request contained no Authorization header")
#         return None
#     else:
#         if 'username' in request.authorization:
#             username = request.authorization['username']
#         else:
#             return None
#         if 'password' in request.authorization:
#             password = request.authorization['password']
#         else:
#             return None
#
#         # first try to authenticate using a token
#         user = User.verify_auth_token(username)
#
#         if user:
#             logger.info("Token verified, login the user and continue.")
#             login_user(user)
#             return user
#         else:
#             # if the token did not work, then use the user/pwd
#             registered_user = User.query.filter_by(username=username).first()
#             if registered_user is None:
#                 logger.info(f"The username {username} is not in the DB")
#                 return None
#             if registered_user.is_correct_password(password):
#                 logger.info(
#                     "user:pwd verified, login the user and continue.")
#                 login_user(registered_user)
#                 return registered_user
#     # finally, return None if both methods did not login the user
#     logger.info(f"The request did not login the user")
#     return None
