import os
from datetime import timedelta
from pathlib import Path

from dotenv import load_dotenv

load_dotenv()


class BasicConfig(object):
    DEBUG = os.getenv("DEBUG")
    TESTING = os.getenv('TESTING')
    TOKEN_EXPIRATION = int(os.getenv('TOKEN_EXPIRATION'))
    SECRET_KEY = os.getenv('SECRET_KEY')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    STORE_FOLDER = Path(os.getenv('STORE_FOLDER'))
    UPLOAD_FOLDER = STORE_FOLDER
    QD_HTML_NAME = os.getenv('QD_HTML_NAME')

    # JWT access tokens config
    # remember to change that to True in production, when using https
    JWT_COOKIE_SECURE = os.getenv('JWT_COOKIE_SECURE')
    JWT_SECRET_KEY = os.getenv("SECRET_KEY_JWT")
    if os.getenv('JWT_ACCESS_TOKEN_EXPIRES'):
        JWT_ACCESS_TOKEN_EXPIRES = timedelta(milliseconds=int(os.getenv(
            'JWT_ACCESS_TOKEN_EXPIRES')))
    # these are fixed
    JWT_TOKEN_LOCATION = ['cookies', 'headers']
    JWT_ACCESS_COOKIE_PATH = '/'
    JWT_REFRESH_COOKIE_PATH = '/auth/refresh'
    JWT_COOKIE_CSRF_PROTECT = True
    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']

    if "DATABASE_PWD_URL" not in os.environ:
        SQLALCHEMY_DATABASE_URI = 'sqlite:///passwd.db'
    else:
        SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_PWD_URL']

    if "DATABASE_PROJECTS_URL" not in os.environ:
        SQLALCHEMY_PROJECTS_URI = 'sqlite:///projects.db'
    else:
        SQLALCHEMY_PROJECTS_URI = os.environ['DATABASE_PROJECTS_URL']

    SQLALCHEMY_BINDS = {
        'projects_db': SQLALCHEMY_PROJECTS_URI
    }
