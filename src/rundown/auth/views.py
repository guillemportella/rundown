from flask import request, jsonify, Blueprint
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_required,
    jwt_refresh_token_required,
    get_jwt_identity,
    get_raw_jwt,
    set_access_cookies, set_refresh_cookies, get_csrf_token, unset_jwt_cookies)

from .helpers import (
    revoke_token,
    is_token_revoked,
    add_token_to_database)
from ..app import jwt, logger
from ..models.users import User

auth_bp = Blueprint('auth', __name__, url_prefix='/auth')


@auth_bp.route('/login', methods=['POST'])
def login():
    """Authenticate user and return token
    """
    logger.debug("Getting a request!")
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)
    logger.debug(f"User name {username}, {password}")
    registered_user = User.query.filter_by(username=username).first()
    if registered_user is None:
        return jsonify({'message': 'User not registered',
                        'authenticated': False}), 401
    if registered_user.is_correct_password(password):
        # create access tokens
        access_token = create_access_token(identity=username)
        refresh_token = create_refresh_token(identity=username)
        add_token_to_database(access_token, 'identity')
        add_token_to_database(refresh_token, 'identity')
        resp = jsonify({
            'login': True,
            'access_csrf': get_csrf_token(access_token),
            'refresh_csrf': get_csrf_token(refresh_token)
        })
        # resp = jsonify({'login': True})
        set_access_cookies(resp, access_token)
        set_refresh_cookies(resp, refresh_token)
        logger.debug(f"Yes! get in!")
        logger.debug("This is my reply:")
        logger.debug(vars(resp))
        return resp, 200
    else:
        return jsonify({'message': 'Invalid credentials',
                        'authenticated': False}), 401


@auth_bp.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    current_user = get_jwt_identity()
    access_token = create_access_token(identity=current_user)
    ret = jsonify({
        'access_csrf': get_csrf_token(access_token),
    })
    set_access_cookies(ret, access_token)
    add_token_to_database(access_token, 'identity')
    return ret, 200


@auth_bp.route('/revoke_access', methods=['DELETE'])
@jwt_required
def revoke_access_token():
    jti = get_raw_jwt()['jti']
    user_identity = get_jwt_identity()
    revoke_token(jti, user_identity)
    return jsonify({"message": f"token revoked for user {user_identity}"}), 200


@auth_bp.route('/revoke_refresh', methods=['DELETE'])
@jwt_refresh_token_required
def revoke_refresh_token():
    jti = get_raw_jwt()['jti']
    user_identity = get_jwt_identity()
    revoke_token(jti, user_identity)
    resp = jsonify({"message": f"token revoked for user {user_identity}"})
    # should i do it here???
    unset_jwt_cookies(resp)
    return resp, 200


@jwt.user_loader_callback_loader
def user_loader_callback(identity):
    registered_user = User.query.filter_by(username=identity).first()
    if registered_user:
        return registered_user
    else:
        return None


@jwt.token_in_blacklist_loader
def check_if_token_revoked(decoded_token):
    return is_token_revoked(decoded_token)
