import logging.config
import os
from collections import namedtuple

from dotenv import load_dotenv

# configure logger
# I need to know if you've set the debug to true, in order to
# supress writing too much in the log file
load_dotenv()

##########################################################################
# Logging configuration
# It should read the 'DEBUG' config from above
##########################################################################

# I just output 'info' level by default, unless debugging
output_level = 'INFO'
if 'DEBUG' in os.environ:
    debug_level = os.getenv('DEBUG')
    if debug_level:
        output_level = 'DEBUG'

LOG_SETTINGS = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': output_level,
            'formatter': 'simple',
            'stream': 'ext://sys.stdout',
        },
        'file_flask': {
            'class': 'logging.handlers.RotatingFileHandler',
            'level': output_level,
            'formatter': 'semidetailed',
            'filename': '/tmp/flask_logger.log',
            'mode': 'a',
            'maxBytes': 10485760,
            'backupCount': 5,
        },

    },
    'formatters': {
        'detailed': {
            'format': '%(asctime)s %(module)-17s line:%(lineno)-4d ' \
                      '%(levelname)-8s %(message)s',
        },
        'email': {
            'format': 'Timestamp: %(asctime)s\nModule: %(module)s\n' \
                      'Line: %(lineno)d\nMessage: %(message)s',
        },
        'simple': {
            'format': '[%(levelname)-7s] %(module)15s : %(message)s',
        },
        'semidetailed': {
            'format': '[%(asctime)s] -' \
                      ' [%(levelname)-7s] %(module)15s : %(message)s',
        }
    },
    'loggers': {
        'flask_logger': {
            'level': output_level,
            'handlers': ['file_flask', ],
            'propagate': False,
        },
        'root': {
            'level': output_level,
            'handlers': ['console']
        },

    },
}

Config = namedtuple('configuration', ['LOG_SETTINGS'])
config_logging = Config(LOG_SETTINGS)
logging.config.dictConfig(config_logging.LOG_SETTINGS, )
