from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.orm import relationship

from ..app import db


class UserDB(db.Model):
    """Define a user class"""
    __tablename__ = "users_db"
    __bind_key__ = "projects_db"

    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column('username', String(64), index=True)
    projects = relationship('Project', back_populates='user')

    html_files = relationship('HtmlFiles', back_populates='user')
    run_name = relationship('RunAnalysis', back_populates='user')

    def __init__(self, username=''):
        self.username = username


class Project(db.Model):
    """Define a project class."""

    __bind_key__ = "projects_db"
    __tablename__ = "projects"
    id = Column(Integer, primary_key=True, autoincrement=True)
    project_name = Column('project_name', String(64))
    user_id = Column(Integer, ForeignKey('users_db.id'), nullable=False)

    user = relationship('UserDB', back_populates='projects')
    run_analysis = relationship("RunAnalysis", back_populates='project')

    def __init__(self, project_name=""):
        """Init the method."""
        self.project_name = project_name
        # self.username = user


class RunAnalysis(db.Model):
    """Define a run analysis class."""

    __tablename__ = "runs"
    __bind_key__ = "projects_db"

    id = Column(Integer, primary_key=True, )
    projects_id = Column(Integer, ForeignKey('projects.id'))
    user_id = Column(Integer, ForeignKey('users_db.id'))
    run_name = Column('run_name', String(64), index=True)

    user = relationship("UserDB", back_populates="run_name")
    project = relationship("Project", back_populates="run_analysis")
    html_files = relationship("HtmlFiles", back_populates="run_analysis")

    def __init__(self, run_name="", ):
        self.run_name = run_name


class HtmlFiles(db.Model):
    """The hmtl files associated to a RunAnalysis"""

    __tablename__ = "html_files"
    __bind_key__ = "projects_db"

    id = Column(Integer, primary_key=True, )
    runanalysis_id = Column(Integer, ForeignKey('runs.id'))
    html_file = Column('html_file', String(64))
    store_name = Column('fname_uuid', String(64))
    user_id = Column(Integer, ForeignKey('users_db.id'), nullable=True)

    user = relationship('UserDB', back_populates='html_files')
    run_analysis = relationship("RunAnalysis", back_populates="html_files")

    def __init__(self, html_file="", fname_uuid=""):
        self.html_file = html_file
        self.store_name = fname_uuid
