from typing import Dict, List

from marshmallow_sqlalchemy import ModelSchema
from sqlalchemy.sql.expression import ClauseElement

from .projects import UserDB, Project, RunAnalysis, HtmlFiles
from ..app import logger

# from ..public.api import api, QueryProjectsAPI

bVerbose = True


class ProjectSchema(ModelSchema):
    class Meta:
        model = Project


class RunAnalysisSchema(ModelSchema):
    class Meta:
        model = RunAnalysis


class HtmlFilesSchema(ModelSchema):
    class Meta:
        model = HtmlFiles


project_schema = ProjectSchema()
run_schema = RunAnalysisSchema()
htmlfiles_schema = HtmlFilesSchema()


def get_or_create(session, model, **kwargs):
    """Create a new instance in model if it does not exist."""
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance, False
    else:
        params = dict((k, v) for k, v in kwargs.items()
                      if not isinstance(v, ClauseElement))
        params.update({})
        instance = model(**params)
        session.add(instance)
        return instance, True


def append_or_create_project(*, session, user_instance, user, project_name):
    """Insert a new project if it does not yet exist"""
    rep_prj = session.query(Project). \
        filter(Project.project_name == project_name). \
        join(UserDB).filter(UserDB.username == user).first()
    if not rep_prj:
        user_instance.projects.append(Project(project_name=project_name))
        logger.debug(f"Appending project name {project_name}")
        session.commit()
    else:
        logger.debug(f"A project {rep_prj.project_name} already existed")


def append_or_create_run(*, session, user, project_name, run_name):
    """Insert a new run in a project if it does not yet exist"""
    rep_run = session.query(RunAnalysis). \
        filter(RunAnalysis.run_name == run_name). \
        join(Project).filter(Project.project_name == project_name). \
        join(UserDB).filter(UserDB.username == user).first()
    if not rep_run:
        rep_prj = session.query(Project). \
            filter(Project.project_name == project_name). \
            join(UserDB).filter(UserDB.username == user).first()
        if rep_prj:
            rep_prj.run_analysis.append(RunAnalysis(run_name=run_name))
            logger.debug(f"Appending run name {run_name}")
            session.commit()
        else:
            logger.error(f"Project {project_name} was supposed to be here")
            raise IOError(f'Can not locate {project_name} in DB.')
    else:
        logger.debug(f"A run name {rep_run.run_name} already existed")


def append_or_create_html(*, session, user, project_name, run_name, file_name,
                          fname_uuid):
    """Add the html file if it's not yet in the DB"""
    user_instance, gc = get_or_create(session, UserDB, username=user)
    rep_html = session.query(HtmlFiles). \
        filter(HtmlFiles.html_file == file_name). \
        join(RunAnalysis).filter(RunAnalysis.run_name == run_name). \
        join(Project).filter(Project.project_name == project_name). \
        join(UserDB).filter(UserDB.username == user).first()
    if not rep_html:
        # find the run to append to
        rep_run = session.query(RunAnalysis). \
            filter(RunAnalysis.run_name == run_name). \
            join(Project).filter(Project.project_name == project_name). \
            join(UserDB).filter(UserDB.username == user).first()
        if rep_run:
            rep_run.html_files.append(HtmlFiles(html_file=file_name,
                                                fname_uuid=fname_uuid))
            # for a reason i have to create that by hand...
            logger.debug(f"Appending html file name {file_name}")
            rep_run.html_files[-1].user_id = user_instance.id
            session.commit()
        else:
            logger.error(f"Run {run_name} was supposed to be here")
            raise IOError(f'Can not locate {run_name} in DB.')

    else:
        logger.debug(f"An html file name {rep_html.file_name} already existed")
        return False


def enter_record(*, db_session=None, user: str = None, form_data: Dict = None,
                 fname_uuid: str = None, ) -> bool:
    """ Create a new record in the database

    The schema must have been validated
    TODO: how do I avoid hardcoding the form_fields here?
    Given a username and a dictionary containing
    all the fields to characterize a project, creates the record.

    The file name of the html file is not the whole path to
    it, only the name of the file. For uniqueness, I use a uuid hex
    as file name.
    This might be a problem if the DB info is lost, as there will
    be no way to recreate it without visually inspecting the files
    and guessing.

    """
    username = user
    project_name = form_data['project_name']
    run_name = form_data['run_name']
    file_name = form_data['html_name']

    # see if we have a user instance already
    logger.debug(f"see if we have a user instance already for {username}")
    user_instance, gc = get_or_create(db_session, UserDB, username=username)
    db_session.commit()

    # insert new project if not yet in project
    try:
        append_or_create_project(session=db_session,
                                 user_instance=user_instance,
                                 user=user,
                                 project_name=project_name
                                 )
    except IOError as e:
        logger.error(e)
        return False

    # insert new run if not yet in user/project
    try:
        append_or_create_run(session=db_session,
                             user=user,
                             project_name=project_name,
                             run_name=run_name,
                             )
    except IOError as e:
        logger.error(e)
        return False

    # insert new html if not yet in user/project
    try:
        append_or_create_html(session=db_session,
                              user=user,
                              project_name=project_name,
                              run_name=run_name,
                              file_name=file_name,
                              fname_uuid=fname_uuid,
                              )
    except IOError as e:
        logger.error(e)
        return False

    return True


def file_belongs_to_user(*, db_session=None, user, filename) -> bool:
    """Query the DB to assert that the filename belongs to the user. """

    query = db_session.query(HtmlFiles).join(UserDB).filter(
        UserDB.username == user).filter(
        HtmlFiles.store_name == filename).first()
    if query is not None:
        return True
    return False


def return_user_projects(*, db_session=None, user) -> List:
    """Return a list of project of the user"""

    query = db_session.query(Project).join(UserDB).filter(
        UserDB.username == user).all()

    reply = []
    for x in query:
        reply.append(x.project_name)

    return reply


def return_project_runs(*, db_session=None, project: str = '',
                        user: str = '') -> List:
    """Return a list of runs for the given project and user"""

    query = db_session.query(RunAnalysis). \
        join(Project).filter(Project.project_name == project). \
        join(UserDB).filter(UserDB.username == user).all()

    reply = []
    for x in query:
        reply.append(x.run_name)
    return reply


def return_run_files(*, db_session, project, user, run) -> Dict:
    """Return a list of files for a given run/project/user

    In this case the return is a dict[file_name, stored_name]
    """

    query = db_session.query(HtmlFiles). \
        join(RunAnalysis).filter(RunAnalysis.run_name == run). \
        join(Project).filter(Project.project_name == project). \
        join(UserDB).filter(UserDB.username == user).all()

    reply = {}
    for x in query:
        reply[x.html_file] = x.store_name
    return reply


def file_exists(*, db_session, user, form_data):
    """Check if the file already exists for that user/project/run"""
    username = user
    project_name = form_data['project_name']
    run_name = form_data['run_name']
    file_name = form_data['html_name']
    logger.debug(f"Check if {project_name} {run_name} {file_name} exists")
    rep = db_session.query(HtmlFiles). \
        filter(HtmlFiles.html_file == file_name). \
        join(RunAnalysis).filter(RunAnalysis.run_name == run_name). \
        join(Project).filter(Project.project_name == project_name). \
        join(UserDB).filter(UserDB.username == user).first()
    if rep:
        logger.debug("Yes it does exist.")
        return True
    else:
        logger.debug("No, it does not exist.")
        return False
