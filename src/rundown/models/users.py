"""Define the user model."""
import re

from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from sqlalchemy.ext.hybrid import hybrid_method

# from ..app import db
from ..app import db, bcrypt
from ..app_config import BasicConfig

TOKEN_EXPIRATION = BasicConfig.TOKEN_EXPIRATION


class User(db.Model):
    """Define a user class."""

    __tablename__ = "users"
    id = db.Column('user_id', db.Integer, primary_key=True)
    username = db.Column('username', db.String(64), unique=True, index=True)
    hashed_password = db.Column('password', db.String(128))

    def __init__(self, username, plaintext_password):
        """Initialize."""
        self.username = username
        self.hashed_password = bcrypt.generate_password_hash(
            password=plaintext_password).decode('utf-8')

    def set_password(self, plaintext_password):
        """Apply bcrypt to password."""
        self.hashed_password = bcrypt.generate_password_hash(
            password=plaintext_password).decode('utf-8')

    @hybrid_method
    def is_correct_password(self, plaintext_password):
        return bcrypt.check_password_hash(self.hashed_password,
                                          plaintext_password)

    def generate_auth_token(self, expiration=TOKEN_EXPIRATION):
        s = Serializer(BasicConfig.SECRET_KEY, expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(BasicConfig.SECRET_KEY)
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        user = User.query.get(data['id'])
        return user

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def __repr__(self):
        return '<User %r>' % (self.username)


def is_valid_username(u):
    """See if username is allowed."""
    reg = re.compile('^[a-zA-Z0-9_.-]+$')
    if len(u) < 3 or len(u) > 14:
        return False
    if not reg.match(u):
        return False
    return True


def is_secure_pwd(pwd):
    """See if secure."""
    if len(pwd) < 8:
        return False
    return True
