import os
import uuid
from pathlib import Path

import werkzeug
from flask import Blueprint, current_app, abort, url_for
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from flask_restplus import Api, Resource, reqparse
from werkzeug.utils import secure_filename

from ..app import db, jwt
from ..app import logger
from ..models import projects_crud as reports_crud

api_bp = Blueprint(
    'api',
    __name__,
    # template_folder='templates',
    # static_folder='static'
)

api = Api(api_bp, version='1.0', title='Rundown API',
          doc='/api/doc',
          prefix="/api/",
          description='A simple API to store QC reports for projects',
          )

# flask-restplus does not communicate the api errors well, so we just
# get an Internal Server Error message from jwt-extended. To get more
# informative messaged, this is the solution so far
jwt._set_error_handler_callbacks(api)
# solution found here
# https://github.com/vimalloc/flask-jwt-extended/issues/86
# this might break in the future, so in the front end one should be
# careful to just accept what is expected

# move to config??
ALLOWED_EXTENSIONS = ["bz2", "gz", ]
QD_HTML_NAME = "qd_show.html"


############################################################################
# helper functions
############################################################################

def allowed_file(filename):
    """Check the allowed file type."""
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def allowed_file_qd(filename):
    """Check the allowed file type."""
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() == "html"


def rename_qd_uploaded_file(*, upfolder: str,
                            fn: str = 'filename.html',
                            fout: str = '') -> bool:
    """Rename the uploaded file to username_QD_HTML_NAME.

    TODO -> avoid directory traversal? Depends on the username.
    """

    p_upload_folder = Path(upfolder)

    complete_path = p_upload_folder / fn
    new_name = p_upload_folder / fout
    # TODO: make sure it does not fail, or capture the error
    try:
        os.replace(complete_path, new_name)
    except OSError as e:
        logger.debug("We could not rename the file, see the error:")
        logger.debug(e)
        return False
    return True


@api_bp.route('/qd_upload')
class QdAPI(Resource):
    """API for quick&dirty uploading of an html file.


    POST -> expects a multipost with a file argument pointing
        to the html file being uploaded.
        Will upload the file to the upload folder and rename it
        according to the user name.

    """
    decorators = [jwt_required]

    def __init__(self, *args, **kwargs):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('file', required=True,
                                   type=werkzeug.datastructures.FileStorage,
                                   help='HTML file not provided',
                                   location='files')
        super(QdAPI, self).__init__()

    def post(self):
        user = get_jwt_identity()
        args = self.reqparse.parse_args()
        file = args['file']
        upload_folder = current_app.config['UPLOAD_FOLDER']
        logger.debug(f"Upload folder is {upload_folder}")

        # run_name = request.form.get('run_name')
        # logger.debug(f"run_name: {run_name}")
        if file and allowed_file_qd(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(upload_folder, filename))
            logger.debug(f"Got the post! {filename}")
            fout = user + "_" + QD_HTML_NAME
            logger.debug(f"fout is: {fout}")
            if rename_qd_uploaded_file(upfolder=upload_folder,
                                       fn=filename,
                                       fout=fout):
                logger.debug("Successfully uploaded")
                return {'status': "OK"}

        else:
            return abort(400, 'File type not allowed or malformed')


@api_bp.route('/upload')
class QCReportAPI(Resource):
    """API for uploadig and storing the record for project-related html files.

    POST -> expects a multipost with the following keys
            project_name
            run_name
            html_file

    The username is also required in the DB, but it will be inferred from the
    user credentials. Only registered users can upload records and files.

    """

    decorators = [jwt_required]

    def __init__(self, *args, **kwargs):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('project_name', required=True,
                                   type=str,
                                   help='project_name not provided',
                                   location='form')
        self.reqparse.add_argument('run_name', required=True,
                                   type=str,
                                   help='run_name not provided',
                                   location='form')
        self.reqparse.add_argument('html_name', required=True,
                                   type=str,
                                   help='html_name not provided',
                                   location='form')
        self.reqparse.add_argument('file', required=True,
                                   type=werkzeug.datastructures.FileStorage,
                                   help='HTML file not provided',
                                   location='files')

        super(QCReportAPI, self).__init__()

    #
    def post(self):
        user = get_jwt_identity()
        args = self.reqparse.parse_args()
        file = args['file']
        fname = args['html_name']

        # we should make sure that it doesn't yet exist
        # if so, don't even bother uploading it
        logger.debug("Will check if file exists")
        if reports_crud.file_exists(db_session=db.session,
                                    user=user,
                                    form_data=args):
            return {'status': "ErrorFileExists",
                    'message': f'File name {fname} already exists'
                               f' in this project/run'}

        # same as qd, make a function of this
        if file and allowed_file_qd(file.filename):
            filename = secure_filename(file.filename)
            upload_folder = current_app.config['UPLOAD_FOLDER']
            logger.debug(f"Upload folder is {upload_folder}")
            file.save(os.path.join(upload_folder, filename))
            logger.debug(f"Got the post project! {filename}")

            the_uuid = uuid.uuid4().hex
            fout = the_uuid + ".html"
            logger.debug(f"Change the original filename to {fout}")
            if rename_qd_uploaded_file(upfolder=upload_folder,
                                       fn=filename,
                                       fout=fout):
                logger.debug("Successfully uploaded")

                # todo: add error checks
                added = reports_crud.enter_record(db_session=db.session,
                                                  user=user,
                                                  form_data=args,
                                                  fname_uuid=the_uuid
                                                  )
                if added:
                    return {'status': "OK", 'stored_fname': fout}
                # this should not happen, but hey... doesn't hurt
                if added:
                    return {'status': "ErrorFileExists",
                            'message': f'File name {fname} already exists'
                                       f' in this project/run'}

        return {'status': "Error"}


@api_bp.route('/projects')
class QueryProjectsAPI(Resource):
    decorators = [jwt_required]

    def __init__(self, *args, **kwargs):
        super(Resource, self).__init__()

    def get(self):
        user = get_jwt_identity()
        logger.debug(f"You seem to be {user}")
        prj = reports_crud.return_user_projects(db_session=db.session,
                                                user=user)
        response = {}
        # I compose the url here because reports_crud doesn't know about them
        for p in prj:
            response[p] = api.url_for(QueryRunsAPI, project=p,
                                      _external=False)
        return response


@api_bp.route('/projects/<string:project>')
class QueryRunsAPI(Resource):
    decorators = [jwt_required]

    def __init__(self, *args, **kwargs):
        super(Resource, self).__init__()

    def get(self, project):
        user = get_jwt_identity()
        rns = reports_crud.return_project_runs(db_session=db.session,
                                               project=project,
                                               user=user,
                                               )
        response = {}
        # I compose the url here because reports_crud doesn't know about them
        for r in rns:
            response[r] = api.url_for(QueryFilesAPI, project=project,
                                      run=r,
                                      _external=False)
        return response


@api_bp.route('/projects/<string:project>/<string:run>')
class QueryFilesAPI(Resource):
    decorators = [jwt_required]

    def __init__(self, *args, **kwargs):
        super(Resource, self).__init__()

    def get(self, project, run):
        user = get_jwt_identity()
        fls = reports_crud.return_run_files(db_session=db.session,
                                            project=project,
                                            user=user,
                                            run=run,
                                            )
        response = {}
        # I compose the url here because reports_crud doesn't know about them
        # <name>/<filename>/<qd>'
        # signature of views_bp.display_report is
        # def display_report(filename, qd):
        # Filename must be the uuid given without the html extension
        # name must match the user, but this is redundant, get rid of it
        for fname, stored_name in fls.items():
            response[fname] = {'url': url_for('views_bp.display_report',
                                              filename=stored_name,
                                              qd='report',
                                              _external=False),
                               'stored_name': stored_name, }
        return response


# register all the endpoints
# it should not be needed
# api.add_resource(TokenAPI, 'get_token')
api.add_resource(QdAPI, 'qd_upload')
# api.add_resource(LoginAPI, 'login')
api.add_resource(QCReportAPI, 'upload')
api.add_resource(QueryProjectsAPI, 'projects')
api.add_resource(QueryRunsAPI, 'projects/<project>')
api.add_resource(QueryFilesAPI, 'projects/<project>/<run>')
