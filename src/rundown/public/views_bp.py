import os
from pathlib import Path

from flask import Blueprint, send_from_directory, current_app, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity

from ..app import db
from ..app import logger
from ..models import projects_crud as reports_crud

views_bp = Blueprint(
    'views_bp',
    __name__,
    template_folder='templates',
    static_folder='static'
)


#############################################################################
# support functions
#############################################################################

def html_stored_file_exists(*, folder: str = "", html: str = "") -> bool:
    """Check if there is a quick&dirty html file"""
    path_html_file = os.path.join(folder, html)
    qd_html_file = Path(path_html_file)
    if qd_html_file.is_file():
        return True
    return False


# should this be part of the API??
@views_bp.route('/user/<filename>/<qd>', methods=['GET'])
@jwt_required
def display_report(filename, qd):
    """Display a given report.

    Filename must be the uuid given without the html extension

    If the user wants to see a quick&dirty upload, the filename
    must match user name. I will build the actual file name here.
    Note that the client is responsible to set the filename right.
    """
    upload_folder = current_app.config['UPLOAD_FOLDER']
    qd_html_name = current_app.config['QD_HTML_NAME']

    name = get_jwt_identity()

    if qd == "True" and name == filename:
        filename = name + "_" + qd_html_name
    elif not reports_crud.file_belongs_to_user(db_session=db.session,
                                               user=name,
                                               filename=filename):
        return jsonify({'message': 'File does not belong to the user'}), 401

    # exists and we have permission to see that given file...
    if qd != "True":
        filename = filename + ".html"
    if html_stored_file_exists(folder=upload_folder, html=filename):
        return send_from_directory(upload_folder, filename)
    else:
        msg = f"QD file {filename} does not exist in "
        msg += f"upload folder {upload_folder}"
        logger.debug(msg)
        return jsonify({'message': 'File not in the database'}), 401
