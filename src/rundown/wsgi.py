from .app import create_app
from .app_config import BasicConfig

app = create_app(BasicConfig)
