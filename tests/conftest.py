import json
import os
import tempfile

import pytest
from rundown.app import create_app
from rundown.app import db as _db
from rundown.app_config import BasicConfig
from rundown.models.users import User

TESTDB_USER = 'test_pwd.db'
TESTDB_USER_PATH = f'/tmp/{TESTDB_USER}'
TESTDB_PROJECTS = 'test_project.db'
TESTDB_PROJECTS_PATH = f'/tmp/{TESTDB_PROJECTS}'
TESTDB_USER_URI = 'sqlite:///' + TESTDB_USER_PATH
TESTDB_PROJECTS_URI = 'sqlite:///' + TESTDB_PROJECTS_PATH

TESTING_USERS = {
    'user1': 'pwd1',
    'user2': 'pwd2'
}


# from http://alexmic.net/flask-sqlalchemy-pytest/
@pytest.fixture(scope='session')
def app(request):
    """Session-wide test `Flask` application."""
    # create a temporary directory to store the files during testing
    with tempfile.TemporaryDirectory() as temp_d:
        settings_override = {
            'TESTING': True,
            'SQLALCHEMY_DATABASE_URI': TESTDB_USER_URI,
            'SQLALCHEMY_BINDS': {"projects_db": TESTDB_PROJECTS_URI},
            'SERVER_NAME': 'local.localhost:8000',
            'STORE_FOLDER': temp_d,
            'UPLOAD_FOLDER': temp_d,
        }
        # overwrite configuration
        for k, v in settings_override.items():
            setattr(BasicConfig, k, v)

        app = create_app(BasicConfig)

        # Flask provides a way to test your application
        # by exposing the Werkzeug test Client
        # and handling the context locals for you.
        testing_client = app.test_client()

        # Establish an application context before running the tests.
        ctx = app.app_context()
        ctx.push()
        yield testing_client  # this is where the testing happens!
    # teardown
    ctx.pop()


@pytest.fixture(scope='session')
def db(app, request):
    """Session-wide test database."""
    if os.path.exists(TESTDB_USER_PATH):
        os.unlink(TESTDB_USER_PATH)

    _db.create_all()

    for u, p in TESTING_USERS.items():
        user = User(u, p)
        _db.session.add(user)

    # Commit the changes for the users
    _db.session.commit()

    yield _db

    # teardown
    _db.drop_all()
    os.unlink(TESTDB_USER_PATH)


@pytest.fixture(scope='function')
def session(db, request):
    """Creates a new database session for a test."""
    connection = db.engine.connect()
    transaction = connection.begin()

    options = dict(bind=connection, binds={})
    session = db.create_scoped_session(options=options)

    db.session = session

    def teardown():
        transaction.rollback()
        connection.close()
        session.remove()

    request.addfinalizer(teardown)
    return session


# TODO --> fixture to create projects/runs etc..


@pytest.fixture(scope='function')
def login_csrf_headers(app):
    data = {
        'username': 'user1',
        'password': 'pwd1',
    }

    rep = app.post('/auth/login',
                   data=json.dumps(data),
                   headers={'Content-Type': "application/json"})
    # double submit CSRF token
    tokens = json.loads(rep.get_data(as_text=True))
    csrf_header_refresh = {'X-CSRF-TOKEN': tokens.get('refresh_csrf')}
    csrf_header_access = {'X-CSRF-TOKEN': tokens.get('access_csrf')}

    return {'access': csrf_header_access, 'refresh': csrf_header_refresh}
