import os
import sys
from typing import List

from flask import json, url_for
from pkg_resources import resource_filename

from tests.conftest import TESTING_USERS

TEST_REPORT = resource_filename(__name__,
                                "testing_report.html")
TEST_REPORT_WRONG = resource_filename(__name__,
                                      "testing_report_wrong.txt")

if len(TESTING_USERS) > 1:
    USER_1, PWD_1 = list(iter(TESTING_USERS.items()))[0]
    USER_2, PWD_2 = list(iter(TESTING_USERS.items()))[1]
else:
    print("TESTING_USERS dictionary must contain at least 2 users! ")
    print("See conftest.py")
    sys.exit(1)


def get_project_user(client, csrf_header, redirect=False):
    return client.get('/api/projects',
                      headers=csrf_header,
                      follow_redirects=redirect
                      )


def get_runs_project_user(client, csrf_headers, project_name='',
                          redirect=False):
    return client.get('/api/projects/' + str(project_name),
                      headers=csrf_headers,
                      follow_redirects=redirect
                      )


def get_files_from_runs(client, csrf_header, project_name='', run_name='',
                        redirect=False):
    xx = ['/api/projects', str(project_name), str(run_name)]
    end_point = "/".join(xx)
    return client.get(end_point,
                      headers=csrf_header,
                      follow_redirects=redirect
                      )


def get_qd_report(client, user, csrf_header):
    endpoint = '/user/' + user + "/True"
    return client.get(endpoint, headers=csrf_header,
                      follow_redirects=True)


def json_of_response(response):
    """Decode json from response"""
    return json.loads(response.data.decode('utf8'))


def post_qd(client, test_fname, csrf_header):
    return client.post('/api/qd_upload',
                       headers=csrf_header,
                       data={'file': open(test_fname, 'rb')},
                       follow_redirects=False
                       )


def post_project(client, test_fname, csrf_header, number=1):
    return client.post('/api/upload',
                       headers=csrf_header,
                       data={'file': open(test_fname, 'rb'),
                             'html_name': 'html_name_' + str(number),
                             'project_name': 'my_project_' + str(number),
                             'run_name': 'run_name_XX',
                             },
                       follow_redirects=False
                       )


def post_project_runs(client, user, test_fname, csrf_header, number=1,
                      nruns=2) -> List:
    reply_status = []
    for nr in range(nruns):
        rv = client.post('/api/upload',
                         headers=csrf_header,
                         data={'file': open(test_fname, 'rb'),
                               'html_name': 'html_name_' + str(number),
                               'project_name': 'my_project_' + str(number),
                               'run_name': 'run_name_' + str(
                                   number) + "_" + str(nr),
                               },
                         follow_redirects=False
                         )
        reply_status.append(rv.status_code)
    return reply_status


def get_user_html(client, stored_fname, csrf_header):
    endpoint = '/user/' + stored_fname + "/placeholder"
    return client.get(endpoint, follow_redirects=True, headers=csrf_header)


def login(client, username, password):
    data = {
        'username': username,
        'password': password,
    }

    return client.post(url_for('auth.login'),
                       data=json.dumps(data),
                       headers={'Content-Type': "application/json"})


def refresh_token(client, test_headers):
    return client.post(url_for('auth.refresh'),
                       headers=test_headers['refresh'])


def check_cookie(response, tokens):
    # Checks for existence of a cookie and checks that is longer than 10
    from werkzeug.http import parse_cookie
    cookies = response.headers.getlist('Set-Cookie')
    found_cookies = {}
    for cookie in cookies:
        for ck, cv in parse_cookie(cookie).items():
            found_cookies[ck] = cv
    for tk in tokens:
        if tk not in found_cookies or len(found_cookies[tk]) < 10:
            print(f"Could not find {tk}")
            return False
    # Cookies might be fine
    return True


def headers_ok(response, tokens):
    """Make sure the header key is in the response"""
    for tk in tokens:
        if tk not in json.loads(response.get_data(as_text=True)):
            print(f"Could not find token {tk}")
            return False
    return True


def revoke_access_token(client, test_headers):
    return client.delete(url_for('auth.revoke_access_token'),
                         headers=test_headers['access'])


def revoke_refresh_token(client, test_headers):
    return client.delete(url_for('auth.revoke_refresh_token'),
                         headers=test_headers['refresh'])


####################################################################
# test the login views
####################################################################

def test_login(app, session):
    """Make sure login works.
    GIVEN a  user/pwd
    WHEN attempting to login using the right values
    THEN check that we get in, and that we get the right cookies
    """
    rv = login(app, USER_1, PWD_1)
    dict_resp = json_of_response(rv)
    assert dict_resp.get('login')

    cookie_tokens_required = ['access_token_cookie', 'csrf_access_token',
                              'refresh_token_cookie', 'csrf_refresh_token']
    header_tokens_required = ['access_csrf', 'refresh_csrf']
    assert check_cookie(rv, cookie_tokens_required)
    assert headers_ok(rv, header_tokens_required)


def test_login_wrong_credentials(app, session):
    """Make sure login works.
    GIVEN a  user/pwd
    WHEN attempting to login using the right values
    THEN check that we get in, and that we get the right cookies
    """
    rv = login(app, USER_1, PWD_2)
    assert rv.status_code == 401

    rv = login(app, USER_2, PWD_1)
    assert rv.status_code == 401


# the original login is done in as a fixture
def test_refresh_token(app, session, login_csrf_headers):
    """Test refreshing login"""
    rv = refresh_token(app, login_csrf_headers)
    assert rv.status_code == 200


def test_logout(app, session, login_csrf_headers):
    # revoke access token
    rv = revoke_access_token(app, login_csrf_headers)
    assert "token revoked for user" in json_of_response(rv)['message']

    # revoke refresh token
    rv = revoke_refresh_token(app, login_csrf_headers)
    assert rv.status_code == 200
    assert "token revoked for user" in json_of_response(rv)['message']


# #############################################################################
# # test the API
# #############################################################################


def test_folder_exists(app, session):
    up_folder = os.getenv("STORE_FOLDER")
    assert os.path.isdir(up_folder)

    sqlite_file = os.getenv("DATABASE_PROJECTS_FILE")
    print(sqlite_file)
    assert os.path.exists(sqlite_file)


def test_post_qd(app, session, login_csrf_headers):
    """Post an html to qd API
    GIVEN registered user & pwd and a report
    WHEN posting a quick&dirty report to the API
    THEN  put the report in the user page
    """
    rv = post_qd(app, TEST_REPORT, login_csrf_headers['access'])
    assert rv.status_code == 200
    assert json_of_response(rv) == {'status': "OK"}

    # post a file with the wrong extension
    rv = post_qd(app, TEST_REPORT_WRONG, login_csrf_headers['access'])
    assert rv.status_code == 400

    # get the actual qd_report
    rv = get_qd_report(app, USER_1, login_csrf_headers['access'])
    assert rv.status_code == 200
    assert b"MultiQC: A modular tool " in rv.data


def test_post_project(app, session, login_csrf_headers):
    """Post a project and check return
    GIVEN registered user/pwd and a project/report
    WHEN posting project information to the projects API
    THEN create a record in project DB, store the file and check return fname
    """

    rv = post_project(app, TEST_REPORT_WRONG, login_csrf_headers['access'])
    assert rv.status_code == 200
    dict_response = json_of_response(rv)
    assert dict_response['status'] == 'Error'

    rv = post_project(app, TEST_REPORT, login_csrf_headers['access'])
    assert rv.status_code == 200
    dict_response = json_of_response(rv)
    assert dict_response['status'] == 'OK'
    assert 'stored_fname' in dict_response
    stored_fname = dict_response['stored_fname']
    filename, file_extension = os.path.splitext(stored_fname)
    assert '.html' in file_extension
    # we could also test that is alpha numeric
    assert len(filename) > 0


def test_post_get_project(app, session, login_csrf_headers):
    """ Post project and check if served by the page
    Less asserts as in test_post_project, they should have failed before

    GIVEN registered user/pwd and a project/report
    WHEN posting project information to the projects API
    THEN create a record in project DB, store the file and check if stored
     """
    # if I don't set a different number, it will complain that the filename
    # is already in the database and won't upload it, as per the functions
    rv = post_project(app, TEST_REPORT, login_csrf_headers['access'], number=2)
    assert rv.status_code == 200
    dict_response = json_of_response(rv)
    stored_fname = dict_response['stored_fname']
    filename, file_extension = os.path.splitext(stored_fname)

    # now we try to fetch it
    rv = get_user_html(app, filename, login_csrf_headers['access'])
    assert rv.status_code == 200
    assert b"MultiQC: A modular tool " in rv.data


def test_runs_projects(app, session, login_csrf_headers):
    """ Post project and check if querying the API for runs works
    GIVEN registered user/pwd and a project/report
    WHEN posting project/runs information to the projects API
    THEN create a record in project DB, store the file and check if stored
     """

    # # create a bunch of projects, and a bunch of runs for each
    for n in range(3):
        l_rv = post_project_runs(app, USER_1, TEST_REPORT,
                                 login_csrf_headers['access'], number=n,
                                 nruns=2)
        for rv in l_rv:
            assert rv == 200

    # get the response json, with projects as key and url as values
    rv = get_project_user(app, login_csrf_headers['access'])
    assert rv.status_code == 200

    # for each key (i.e. project name), query the runs
    # I don't test for the url, though... endpoint hardcoded
    for prj in json_of_response(rv):
        rv = get_runs_project_user(app, login_csrf_headers['access'],
                                   project_name=prj)
        assert rv.status_code == 200


def test_duplicated_files(app, session, login_csrf_headers):
    """ Check if the API refuses to store duplicated records
    GIVEN registered user/pwd and a project/report
    WHEN posting two exact project/runs/fname information to the projects API
    THEN check if the API refuses to store it and warns the user
     """
    # check it complains if file exists
    # first post one
    rv = post_project(app, TEST_REPORT, login_csrf_headers['access'],
                      number=33)
    assert rv.status_code == 200
    dict_response = json_of_response(rv)
    assert dict_response['status'] == 'OK'

    # do it again, it should complain
    rv = post_project(app, TEST_REPORT, login_csrf_headers['access'],
                      number=33)
    assert rv.status_code == 200
    dict_response = json_of_response(rv)
    assert dict_response['status'] == 'ErrorFileExists'


def test_files_from_run(app, session, login_csrf_headers):
    """ Post project and check if API returns the files names for a run
    GIVEN registered user/pwd and a project/report
    WHEN posting project/runs/fname information to the projects/runs/fname API
    THEN the API should reply with filenames and their URL
     """
    for n in range(2):
        l_rv = post_project_runs(app, USER_1, TEST_REPORT,
                                 login_csrf_headers['access'], number=n,
                                 nruns=2)
        for rv in l_rv:
            assert rv == 200

    rv = get_project_user(app, login_csrf_headers['access'])
    assert rv.status_code == 200
    for prj in json_of_response(rv):
        rns = get_runs_project_user(app, login_csrf_headers['access'],
                                    project_name=prj)
        assert rv.status_code == 200
        assert len(rv.data)
        response = json_of_response(rns)
        if len(response) and 'Error' not in response:
            for run in response:
                fns = get_files_from_runs(app, login_csrf_headers['access'],
                                          project_name=prj,
                                          run_name=run)
                assert fns.status_code == 200
                assert len(json_of_response(fns)) > 0
                for fname, val in json_of_response(fns).items():
                    assert 'stored_name' in val
                    stored_name = val['stored_name']
                    rv = get_user_html(app, stored_name,
                                       login_csrf_headers['access'])
                    assert rv.status_code == 200
                    assert b"MultiQC: A modular tool " in rv.data
