#! /usr/bin/env python3
"""Define the user model."""
import argparse
import os
from argparse import RawDescriptionHelpFormatter
from typing import Dict

import bcrypt
from sqlalchemy import Column, String, Integer, ForeignKey, Boolean, DateTime
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

__doc__ = """
Submit the reports to the 'rundown' API
"""

DEFAULT_PATH_DATABASE = 'passwd.db'

DESC_MAIN = """\n\
Store user and password in a database.

If the database exists the default behaviour is to append to it. You can 
overwrite it by setting the -overwrite flag.

"""
whowhen = "Guillem Portella, v0.1, 08-2018"

Base = declarative_base()


def parse_arguments() -> Dict:
    """Parse the arguments.

    rtype: arguments dictionary
    """
    parser = argparse.ArgumentParser(
        description=DESC_MAIN,
        epilog=whowhen,
        formatter_class=RawDescriptionHelpFormatter,
    )
    parser.add_argument(
        '-ov',
        '--overwrite',
        default=False,
        action='store_true',
        help="Overwrite database if one with the same name already exists.")
    parser.add_argument(
        "-db",
        "--db_name",
        type=str,
        required=False,
        default=DEFAULT_PATH_DATABASE,
        help="The name of the database.",
    )
    parser.add_argument(
        "-u", "--username", metavar="user", type=str, help="The user name."
    )
    parser.add_argument(
        "-p",
        "--password",
        type=str,
        help="The user password.",
    )

    args = vars(parser.parse_args())
    return args


class User(Base):
    """Define a user class."""

    __tablename__ = "users"

    id = Column("user_id", Integer, primary_key=True)
    username = Column("username", String(64), unique=True, index=True)
    _password = Column("password", String(128))

    def __init__(self, username, plaintext_password):
        """Initialize."""
        self.username = username
        self.set_password(plaintext_password)

    def set_password(self, plaintext_password):
        """Apply bcrypt to password."""
        self._password = bcrypt.hashpw(
            plaintext_password.encode("utf8"), bcrypt.gensalt()
        )

    def is_correct_password(self, plaintext_password):
        if bcrypt.checkpw(plaintext_password.encode("utf8"), self._password):
            return True
        else:
            return False


class TokenBlacklist(Base):
    """Blacklist representation
    """
    __tablename__ = 'blacklist'
    id = Column(Integer, primary_key=True)
    jti = Column(String(36), nullable=False, unique=True)
    token_type = Column(String(10), nullable=False)
    user_id = Column(Integer, ForeignKey('users.user_id'), nullable=False)
    revoked = Column(Boolean, nullable=False)
    expires = Column(DateTime, nullable=False)

    user = relationship('User', lazy='joined')

    def to_dict(self):
        return {
            'token_id': self.id,
            'jti': self.jti,
            'token_type': self.token_type,
            'user_identity': self.user_identity,
            'revoked': self.revoked,
            'expires': self.expires
        }

def get_or_create(l_session, model, *, user: str = "guillem",
                  pwd: str = "x123xy89x"):
    """Create a new instance in model if it does not exist."""
    instance = l_session.query(model).filter_by(username=user).first()
    if instance:
        return instance, False
    else:
        params = {"username": user, "plaintext_password": pwd}
        params.update({})
        instance = model(**params)
        session.add(instance)
        return instance, True


def check_pwd(l_session, model, *, user: str = "guillem",
              pwd: str = "x123xy89x"):
    registered_user = l_session.query(model).filter_by(username=user).first()
    if registered_user:
        if not registered_user.is_correct_password(pwd):
            print(f"Error: pwd for user {user} could not be stored.")
    else:
        print(f"Error: could not store user {user}")


if __name__ == "__main__":
    # parse the arguments
    arguments = parse_arguments()
    username = arguments["username"]
    password = arguments["password"]
    path_database = arguments["db_name"]

    if arguments['overwrite']:
        try:
            os.remove(path_database)
        except OSError as e:
            print(f"Warning: can not remove database {path_database}")
            print(e)
            print("Going ahead anyway.")

    # generate database schema
    database = "sqlite:///" + path_database
    engine_users = create_engine(database)
    Session_users = sessionmaker(engine_users)
    Base.metadata.create_all(engine_users)
    # create a new session
    session = Session_users()
    session.commit()

    _, gc = get_or_create(session, User, user=username, pwd=password)

    if gc:
        # check passwords
        check_pwd(session, User, user=username, pwd=password)
        session.commit()
        session.close()
    else:
        print("ERROR: username already exists. Use -ov to overwrite.")
    exit()
