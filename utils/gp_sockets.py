#! /user/bin/env python

import asyncio
import functools
import json
import random

import websockets
from aiohttp import web, web_runner
from yarl import URL

DESC_MAIN = """
AIOHTTP server to respond to POST requests via querying another
computer using websockets. My use case is this: 
I can't establish a server in the computer clusters, but I can create
a websocket client to a remote server. This is the remote server.

This remote server accepts POST requests and forwards this to the 
websocket client. The websocket client performs the action, sends the
reply back to this server, and this server back to the original 
POST request. 

The WS connection is WSS using self-signed SSL certificates. To start
the application you might need to input the password associated with
the certificates.

The list of accepted post commands is listed below. Any other command
is dealt with an error message in a json.
"""
whowhen = 'Guillem Portella, v0.1, 09-2018'

allowed_queries = ['queue_state', 'live!']

"""The reason we are monkey patching here, and using all the 
web.Apprunner later on, is to be base to get hold of the eventloop.
Maybe there's a better way....
"""


class TCPSite_patched(web_runner.BaseSite):
    """
    this is a copy paste of the TCPSite code except where shown
    """
    __slots__ = ('_host', '_port', '_reuse_address', '_reuse_port')

    def __init__(self, runner, host=None, port=None, *,
                 shutdown_timeout=60.0, ssl_context=None,
                 backlog=128, reuse_address=None,
                 reuse_port=None,
                 # >>> added this
                 loop=None
                 ):
        super().__init__(runner, shutdown_timeout=shutdown_timeout,
                         ssl_context=ssl_context, backlog=backlog)
        # >>> added this
        if loop is None:
            loop = asyncio.get_event_loop()
        self.loop = loop
        #
        if host is None:
            host = "127.0.0.1"
        self._host = host
        if port is None:
            port = 8443 if self._ssl_context else 8083
        self._port = port
        self._reuse_address = reuse_address
        self._reuse_port = reuse_port

    @property
    def name(self):
        scheme = 'https' if self._ssl_context else 'http'
        return str(URL.build(scheme=scheme, host=self._host, port=self._port))

    async def start(self):
        await super().start()
        self._server = await self.loop.create_server(
            #
            self._runner.server, self._host, self._port,
            ssl=self._ssl_context, backlog=self._backlog,
            reuse_address=self._reuse_address,
            reuse_port=self._reuse_port)


# ----
# below is sample code


class WebServer(object):
    def __init__(self, address='127.0.0.1', port=8083,
                 loop=None, queue=None, q_out=None):
        self.address = address
        self.port = port
        self.queue = queue
        self.qout = q_out
        if loop is None:
            loop = asyncio.get_event_loop()
        self.loop = loop
        asyncio.ensure_future(self.start(), loop=self.loop)

    async def start(self):
        self.app = web.Application(loop=self.loop, debug=True)
        self.setup_routes()
        self.runner = web.AppRunner(self.app)
        await self.runner.setup()
        self.site = TCPSite_patched(self.runner,
                                    self.address, self.port,
                                    loop=self.loop)
        await self.site.start()
        print('------ serving on %s:%d ------'
              % (self.address, self.port))

    def setup_routes(self):
        self.app.router.add_post('/query', self.index)

    async def index(self, request):
        # add a query to the incoming queue based on the post
        # puts the query in the queue of websocket messages to be sent out
        # a message should be past, and the reply should be added
        # by our consumer to qout
        # we therefore await for the qout to return a message. To prevent locks
        # when the connection is not up we TimeOut after 5 s
        data = await request.json()
        if "query" in data:
            if data['query'] in allowed_queries:
                await self.queue.put({"query": data['query']})
            try:
                xx = await asyncio.wait_for(self.qout.get(), 5.0)
                # could be that a keep alive message was lurking in
                # the reply message from our keep alive is "live!"
                while "live!" in json.loads(xx)['reply']:
                    xx = await asyncio.wait_for(self.qout.get(), 1)

                return web.json_response(json.loads(xx))

            except asyncio.TimeoutError as e:
                return web.json_response({'error': "TimeOut"})
        else:
            return web.json_response({'error': "Bad format POST"})


async def consumer_handler(queue, websocket):
    while True:
        async for message in websocket:
            await queue.put(message)


async def producer_g(queue, websocket):
    while True:
        # wait for an item from the producer
        item = await queue.get()

        # process the item
        logging.info('consuming {}...'.format(item))
        await websocket.send(json.dumps(item))
        # simulate i/o operation using sleep
        await asyncio.sleep(random.random())

        # Notify the queue that the item has been processed
        queue.task_done()


async def handler(websocket, path, queue, q_out):
    keep = asyncio.ensure_future(keep_websocket(queue))
    producer_task = asyncio.ensure_future(producer_g(queue, websocket))
    consumer_task = asyncio.ensure_future(consumer_handler(q_out, websocket))
    done, pending = await asyncio.wait(
        [keep, producer_task, consumer_task],
        return_when=asyncio.ALL_COMPLETED,
    )

    for task in pending:
        task.cancel()


class LoopTester(object):
    def __init__(self, loop=None, queue=None, q_out=None):
        if loop is None:
            loop = asyncio.get_event_loop()
        self.loop = loop
        self.counter = 0
        self.queue = queue
        self.q_out = q_out
        # ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
        # ssl_context.load_cert_chain(
        #     pathlib.Path(__file__).with_name('cert.pem'),
        #     pathlib.Path(__file__).with_name('key.pem'))
        new_handler = functools.partial(handler,
                                        queue=self.queue,
                                        q_out=self.q_out)
        start_server = websockets.serve(
            #    new_handler, 'localhost', 8765, ssl=ssl_context)
            new_handler, 'localhost', 8765, )
        asyncio.ensure_future(start_server, loop=self.loop)


async def keep_websocket(queue):
    while True:
        await asyncio.sleep(3600)
        await queue.put({"query": 'live!'})


if __name__ == '__main__':
    import logging

    incoming = asyncio.Queue()
    outgoing = asyncio.Queue()
    loop = asyncio.get_event_loop()
    logging.basicConfig(level=logging.DEBUG)
    loop.set_debug(False)
    ws = WebServer(loop=loop, queue=incoming, q_out=outgoing)
    lt = LoopTester(loop=loop, queue=incoming, q_out=outgoing)
    # keep_alive = keep_websocket(queue=incoming)
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        tasks = asyncio.gather(
            *asyncio.Task.all_tasks(loop=loop),
            loop=loop,
            return_exceptions=True)
        tasks.add_done_callback(lambda t: loop.stop())
        tasks.cancel()
