#!/usr/bin/env python

# WS  client example, with a self-signed certificate
import argparse
import json
import os
import pathlib
import ssl
from os import listdir
from os.path import isfile, join
from typing import Dict

import websockets

# just for testing
cwd = os.getcwd()
onlyfiles = [f for f in listdir(cwd) if isfile(join(cwd, f))]

WS_HOST = "ws://localhost:8765"
DESC_MAIN = '''\n\
Websocket client daemon. 

Establish a websocket connection and reply based on a pre-stablished
set of queries. 
'''
whowhen = 'Guillem Portella, v0.1, 09-2018'

import asyncio


async def run_command(*args):
    """Run command in subprocess

    Example from:
        http://asyncio.readthedocs.io/en/latest/subprocess.html
    """
    # Create subprocess
    process = await asyncio.create_subprocess_exec(
        *args,
        # stdout must a pipe to be accessible as process.stdout
        stdout=asyncio.subprocess.PIPE)

    # Status
    print('Started:', args, '(pid = ' + str(process.pid) + ')')

    # Wait for the subprocess to finish
    stdout, stderr = await process.communicate()

    # Progress
    if process.returncode == 0:
        print('Done:', args, '(pid = ' + str(process.pid) + ')')
    else:
        print('Failed:', args, '(pid = ' + str(process.pid) + ')')

    # Result
    result = stdout.decode().strip()

    # Return stdout
    return result


async def run_command_shell(command):
    """Run command in subprocess (shell)

    Note:
        This can be used if you wish to execute e.g. "copy"
        on Windows, which can only be executed in the shell.
    """
    # Create subprocess
    process = await asyncio.create_subprocess_shell(
        command,
        stdout=asyncio.subprocess.PIPE)

    # Status
    print('Started:', command, '(pid = ' + str(process.pid) + ')')

    # Wait for the subprocess to finish
    stdout, stderr = await process.communicate()

    # Progress
    if process.returncode == 0:
        print('Done:', command, '(pid = ' + str(process.pid) + ')')
    else:
        print('Failed:', command, '(pid = ' + str(process.pid) + ')')

    # Result
    result = stdout.decode().strip()

    # Return stdout
    return result


async def process_message(message: dict):
    """Act on the message"""

    if 'query' not in message:
        return json.dumps({'status': "Error, query not in message"})
    else:
        if message['query'] == "queue_state":
            what = await run_command('ls')
            return json.dumps({'status': 'ok',
                               'reply': what.split()})
        elif message['query'] == "live!":
            return json.dumps({'status': 'ok',
                               'reply': "live!"})
        else:
            print("Not queue_state")
            return json.dumps({'status': 'ok',
                               'reply': 'Could not find queue state'})


class DataReader:

    def __init__(self, *, address=None, ssl=None):
        self.address = address
        self.socket = None
        self.ssl = ssl

    async def create_socket(self):
        self.socket = await websockets.connect(self.address,
                                               ssl=self.ssl)

    async def send(self, message):
        await  self.socket.send(message)

    async def receive(self):
        return await self.socket.recv()

    def on_message(self, message):
        print(message)

    def start(self):
        asyncio.async(self.start_async())

    async def start_async(self):
        try:
            self.socket = await websockets.connect(self.address, )
            #              ssl=self.ssl)
            while True:
                message = await self.socket.recv()
                self.on_message(message)
                reply = await process_message(json.loads(message))
                await  self.socket.send(reply)

        except Exception as e:
            print(e)
            self.start()


def parse_arguments() -> Dict:
    """Parse the arguments.

    rtype: arguments dictionary
    """
    parser = argparse. \
        ArgumentParser(description=DESC_MAIN,
                       epilog=whowhen,
                       formatter_class=argparse.RawDescriptionHelpFormatter, )
    parser.add_argument(
        '-u',
        metavar='url_ws', type=str,
        default=WS_HOST,
        required=False,
        help='The URL of the websocket to connect to')

    # parse args and call the right function
    args = vars(parser.parse_args())
    return args


if __name__ == "__main__":
    args = parse_arguments()
    url = args['u']
    ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
    ssl_context.load_verify_locations(
        pathlib.Path(__file__).with_name('cert.pem'))

    reader = DataReader(address=url, ssl=ssl_context)
    reader.start()
    asyncio.get_event_loop().run_forever()
